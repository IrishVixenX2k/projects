#include "Enemy.h"

Enemy::Enemy()
{

}

//sets stats for game
Enemy::Enemy(int health, int stamina, int luck, int dmg, 
	int persuasion, int intimidation, int deceit, char typeID)
{
	//spawn(typeID);
	m_HealthP = new int(health);
	m_StaminaP = new int(stamina);
	m_LuckP = new int(luck);
	m_DmgP = new int(dmg);
	m_PersuasionP = new int(persuasion);
	m_IntimidationP = new int(intimidation);
	m_DeceitP = new int(deceit);
}

//spawns an object at a random position on the map and places their type
void Enemy::spawn(char typeID)
{
	m_XP = new int(rand() % SIZE);
	m_YP = new int(rand() % SIZE);
	m_TypeIDP = new char(typeID);
}

//displays the position of each object
void Enemy::draw()
{
	if (*m_TypeIDP == 'P')
	{
		cout << "Your Position: (" << *m_XP << ", " << *m_YP << ")" << endl;
	}

}

//updates the map
void Enemy::update(char map[SIZE][SIZE])
{

}

bool Enemy::isAlive()//check if object is alive
{
	if (*m_HealthP > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int Enemy::getHealth(){return *m_HealthP;}
int Enemy::getStamina(){return *m_StaminaP;}
int Enemy::getLuck(){return *m_LuckP;}
int Enemy::getDmg(){return *m_DmgP;}
int Enemy::getPersuasion(){return *m_PersuasionP;}
int Enemy::getIntimidation(){return *m_IntimidationP;}
int Enemy::getDeceit(){return *m_DeceitP;}

Enemy::~Enemy()//destructor
{
	*m_HealthP = 0;
	*m_StaminaP = 0;
	*m_LuckP = 0;
	*m_DmgP = 0;
	*m_PersuasionP = 0;
	*m_IntimidationP = 0;
	*m_DeceitP = 0;
	//*m_XP = 0;
	//*m_YP = 0;
	//*m_TypeIDP = NULL;
}
