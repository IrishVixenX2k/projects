//
//  Turret.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 29/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef __SDL_Game_Programming_Book__Turret__
#define __SDL_Game_Programming_Book__Turret__

#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"

class Blob : public Enemy
{
public:

	Blob()
	{
		m_dyingTime = 1000;
		m_health = 7;
		m_moveSpeed = 1;
		m_bulletFiringSpeed = 200;
	}

	virtual ~Blob() {}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());
			m_velocity.setX(-m_moveSpeed);

			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet2", 1, Vector2D(-3, 0));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 20, m_position.getY(), 16, 16, "bullet2", 1, Vector2D(0, -3));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 40, m_position.getY(), 16, 16, "bullet2", 1, Vector2D(3, 0));
				m_bulletCounter = 0;
			}
			m_bulletCounter++;

		}
		else
		{
			m_velocity.setY(0);
			doDyingAnimation();
		}

		ShooterObject::update();
	}


};

class BlobCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Blob();
	}
};


#endif /* defined(__SDL_Game_Programming_Book__Turret__) */
