#pragma once
#ifndef ENEMY_H
#define ENEMY_H
//#define SIZE 26
//#define FREE 'x'
#include <iostream>
#include "GameObject.h"
#include <ctime>
#include <string>

using namespace std;

class Enemy: public GameObject {
public:
	Enemy();
	//Constructor
	Enemy(int health, int stamina, int luck, int dmg, int persuasion, int intimidation, int deceit, char typeID);

	//function declarations
	void spawn(char typeID);//stores object type and x/y position
	void draw();
	virtual void update(char map[SIZE][SIZE]);//prints map
	void info();

	bool isAlive();//checks if object is alive

				   //getters for use in game class
	//char getId();
	//int getX();
	//int getY();
	int getHealth();
	int getStamina();
	int getLuck();
	int getDmg();
	int getPersuasion();
	int getIntimidation();
	int getDeceit();

	//destructor
	~Enemy();


	//protected variables/pointers
protected:
	//stats for object
	int* m_HealthP;
	//int* m_XP;
	//int* m_YP;
	int* m_StaminaP;
	int* m_LuckP;
	int* m_DmgP;
	int* m_PersuasionP;
	int* m_IntimidationP;
	int* m_DeceitP;
	//char* m_TypeIDP;
};
#endif