#include "GameObject.h"

//sets stats for game
GameObject::GameObject(char typeID)
{
	spawn();
	m_TypeIDP = new char(typeID);
}

//spawns an object at a random position on the map and places their type
void GameObject::spawn()
{
	m_XP = new int(rand() % SIZE);
	m_YP = new int(rand() % SIZE);
}

//displays the position of each object
/*void GameObject::draw()
{
	if (*m_TypeIDP == 'P')
	{
		cout << "Your Position: (" << *m_XP << ", " << *m_YP << ")" << endl;
	}

}*/

//updates the map
void GameObject::update(char map[SIZE][SIZE]){}

//getters for stats used in game class
char GameObject::getId(){return *m_TypeIDP;}
int GameObject::getX(){return *m_XP;}
int GameObject::getY(){return *m_YP;}

//destructor
GameObject::~GameObject()
{*m_XP = 0; *m_YP = 0; *m_TypeIDP = NULL;}
