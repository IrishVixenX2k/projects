//
//  Level1Boss.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 31/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef LEVEL2BOSS_H
#define LEVEL2BOSS_H

#include "Enemy.h"
#include "Game.h"
#include "TextureManager.h"

class Level2Boss : public Enemy
{
public:

	virtual ~Level2Boss() {}

	Level2Boss() : Enemy()
	{
		m_health = 1/*00*/;
		m_dyingTime = 100;
		m_bulletFiringSpeed = 150;

		m_moveSpeed = 2;

		m_entered = false;
	}

	virtual void load(std::unique_ptr<LoaderParams> const &pParams)
	{
		ShooterObject::load(std::move(pParams));

		m_velocity.setX(-m_moveSpeed);
	}

	virtual void collision()
	{
		if (m_entered)
		{
			m_health -= 1;

			if (m_health == 0)
			{
				if (!m_bPlayedDeathSound)
				{
					m_position.setX(m_position.getX() + 30);
					m_position.setY(m_position.getY() + 70);
					TheSoundManager::Instance()->playSound("explode", 0);

					m_textureID = "bossexplosion";
					m_currentFrame = 0;
					m_numFrames = 9;
					m_width = 180;
					m_height = 180;
					m_bDying = true;
				}

			}
		}
	}

	virtual void update()
	{
		if (!m_entered)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

			if (m_position.getX() < (TheGame::Instance()->getGameWidth() - (m_width + 20)))
			{
				m_entered = true;
			}
		}
		else
		{
			if (!m_bDying)
			{
				/*if (m_bossStopCount >= 104)
				{
					m_bossStopCount = 0;
				}*/
				if (m_position.getX() + m_width >= TheGame::Instance()->getGameWidth() /*- 40*//* && m_bossStopCount <= 100*/)
				{
					m_velocity.setX(-m_moveSpeed);
					
					m_textureID = "bosswalkleft";
					m_currentFrame = 0;
					m_numFrames = 4;
					m_width = 250;
					m_height = 250;
				}
				else if (m_position.getX() <= 0 /*&& m_bossStopCount <= 100*/)
				{
					m_velocity.setX(m_moveSpeed);
					
					m_textureID = "bosswalkright";
					m_currentFrame = 0;
					m_numFrames = 4;
					m_width = 250;
					m_height = 250;
				}

				//else if (m_bossStopCount >= 100)
				//{
				//	if (m_bulletCounter == m_bulletFiringSpeed)
				//	{
				//		m_velocity.setX(0);

				//		//std::cout << "boss stoping to fire\n";

				//		m_textureID = "boss";
				//		m_currentFrame = 0;
				//		m_numFrames = 1;
				//		m_width = 250;
				//		m_height = 250;

				//		//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 100, 16, 16, "bullet2", 1, Vector2D(-10, 0));
				//		TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 125, 16, 16, "bullet2", 1, Vector2D(-10, 0));

				//		// TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 200, 16, 16, "bullet2", 1, Vector2D(-10, 0));
				//		//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 215, 16, 16, "bullet2", 1, Vector2D(-10, 0));

				//		m_bulletCounter = 0;
				//	}
				//}

				if (m_bulletCounter == m_bulletFiringSpeed)
				{
					TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 125, m_position.getY() + m_height - 90, 32, 32, "fireball", 4, Vector2D(-6, 0));
					TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 125, m_position.getY() + m_height - 90, 32, 32, "fireball", 4, Vector2D(-4, -3));
					TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 125, m_position.getY() + m_height - 90, 32, 32, "fireball", 4, Vector2D(-2, -5));
					TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 125, m_position.getY() + m_height - 90, 32, 32, "fireball", 4, Vector2D(0, -6));
				    TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 125, m_position.getY() + m_height - 90, 32, 32, "fireball", 4, Vector2D(2, -5));
					TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 125, m_position.getY() + m_height - 90, 32, 32, "fireball", 4, Vector2D(4, -3));
					TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 125, m_position.getY() + m_height - 90, 32, 32, "fireball", 4, Vector2D(6, 0));

					m_bulletCounter = 0;
				}

				m_bulletCounter++;
				//++m_bossStopCount;

				ShooterObject::update();
			}
			else
			{
				scroll(TheGame::Instance()->getScrollSpeed());
				m_currentFrame = int(((SDL_GetTicks() / (1000 / 3)) % m_numFrames));

				if (m_dyingCounter == m_dyingTime)
				{
					m_bDead = true;
					TheGame::Instance()->setLevelComplete(true);
				}
				m_dyingCounter++;

			}

		}
	}

private:

	bool m_entered;
	int m_bossStopCount = 0;
};

class Level2BossCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Level2Boss();
	}
};


#endif
