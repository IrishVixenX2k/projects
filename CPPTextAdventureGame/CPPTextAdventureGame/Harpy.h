#ifndef HARPY_H
#define HARPY_H
#include "Enemy.h"
#include "Game.h"

class Harpy : public Enemy
{
public:
	//sets harpy ID to H
	Harpy(char typeID = 'H');
	//updates the harpy position on the map
	virtual void update(char[SIZE][SIZE]);
};
#endif



