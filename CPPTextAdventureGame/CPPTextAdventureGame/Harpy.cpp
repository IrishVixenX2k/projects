#include "Harpy.h"

Harpy::Harpy(char typeID)//constructor
{
	spawn(typeID);
	m_HealthP = new int(35);
	m_StaminaP = new int(10);
	m_LuckP = new int(3);
	m_DmgP = new int(10);
	m_PersuasionP = new int(1);
	m_IntimidationP = new int(2);
	m_DeceitP = new int(3);
}

void Harpy::update(char map[SIZE][SIZE]){}