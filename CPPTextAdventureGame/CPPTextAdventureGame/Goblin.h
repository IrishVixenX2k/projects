#pragma once
#ifndef GOBLIN_H
#define GOBLIN_H
#include "Enemy.h"
#include "Game.h"

class Goblin : public Enemy
{
public:
	//sets goblin ID to G
	Goblin(char typeID = 'G');
	//updates the goblin position on the map
	virtual void update(char[SIZE][SIZE]);
};
#endif



