#pragma once
#ifndef GAME_H
#define GAME_H
//#define SIZE 30
#define FREE '"'
#include "Player.h"
#include "Goblin.h"
#include "Harpy.h"
#include "Bandit.h"
#include "Chest.h"
#include <vector>
#include <iostream>
#include <stdbool.h>

using namespace std;

class Game
{
public:

	//function declarations
	void init();
	void draw();

	void talk();
	void battle();

	void update();//update function for objects a day/night cycle
	void info();//prints game information
	void printMap();//prints the map

	static bool check(int x, int y);//checks if move is valid

	void checkChest();//checks if player is on chest position
	void chestRiddle();//picks a random riddle for unlocking chests
	void chestReward();//picks reward the player gets

	bool isTalking();//checks if player is in dialogue
	bool isBattling();//checks if player is battling
	bool isNight();//checks if it is night or day
	bool getVictory();//if player victorious

	void playerDodge();//dodge function for battle
	void enemyAttack();//enemy attack function for battle
	void playerAttack();//player standard attack for battle
	void playerSpecialAttack();//player special attack for battle

	void harpyTalk();//dialogue for harpy
	void goblinTalk();//dialogue for goblin
	void banditTalk();//dialogue for bandit

//private variables
private:
	Enemy *player = NULL;//used for finding player in array
	char map[SIZE][SIZE];//used for printing map

	//vectors and iterators for objects
	vector<Enemy*>::iterator iterEnemies;
	vector<Enemy*> enemies;
	vector<GameObject*>::iterator iterChest;
	vector<GameObject*> chests;

	//player statistics
	int playerHealth;
	int playerBattleHealth;
	int playerStamina;
	int playerLuck;
	int playerPersuasion;
	int playerIntimidation;
	int playerDeceit;
	int playerDmg;

	//enemy statistics
	int enemyHealth;
	string enemyType;
	int enemyDmg;

	//for special attacks
	int dmgMultiplier;

	//for checking if conditions are true
	bool battling = false;//battle
	bool talking = false;//talk
	bool night = false;//isNight
	bool correct = false;//chest riddle answered correct
	bool incorrect = false;//chest riddle answered incorrect

	int deadCount;

	bool victory;

	//day and night cycle
	int hour;
	int newHour;
};

#endif

