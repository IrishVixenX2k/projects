#include "Player.h"
Game game;

//sets stats for player
Player::Player(char typeID)//constructor
{
	spawn(typeID);
	m_HealthP = new int(100);
	m_StaminaP = new int(3);
	m_LuckP = new int(3);
	m_DmgP = new int(5);
	m_PersuasionP = new int(1);
	m_IntimidationP = new int(1);
	m_DeceitP = new int(1);
}

void Player::update(char map[SIZE][SIZE])//player update func
{
	if (*m_HealthP > 0) {//player movement
		char direction;
		cout << "Player: \n";
		cout << "w = up, s = down, a = left, d = right, i = player information\n";
		cout << "Enter your direction ->";
		cin >> direction;
		int updateX;
		int updateY;
		map[*m_XP][*m_YP] = FREE;

		if (direction == 'w') //move up
		{
			updateX = *m_XP;
			updateY = *m_YP - 1;
		}
		else if (direction == 'a') //move left
		{
			updateX = *m_XP - 1;
			updateY = *m_YP;
		}
		else if (direction == 's') //move down
		{
			updateX = *m_XP;
			updateY = *m_YP + 1;
		}
		else if (direction == 'd') //move right
		{
			updateX = *m_XP + 1;
			updateY = *m_YP;
		}
		else if (direction == 'i')//print player info
		{
			updateX = *m_XP;
			updateY = *m_YP;
			game.info();
		}
		else
		{
			updateX = *m_XP;
			updateY = *m_YP;
		}

		if (Game::check(updateX, updateY) == true) { //checks if move is valid

			*m_XP = updateX;
			*m_YP = updateY;
			map[*m_XP][*m_YP] = *m_TypeIDP;
			//destroys player if health is below 1
			if (*m_HealthP <= 0) {
				Enemy::~Enemy();
			}

		}
	}
}