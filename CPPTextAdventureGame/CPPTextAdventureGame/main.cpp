#include "Player.h"
#include "Game.h"
#include <ctime>

int main()
{
	srand(static_cast<unsigned int>(time(NULL)));
	Game game;
	Player player;
	bool victorious = false;
	bool talking = false;
	bool battling = false;
	game.init();

	while (victorious == false)
	{
		game.getVictory();
		victorious = game.getVictory();
		game.isBattling();
		battling = game.isBattling();
		game.isTalking();
		talking = game.isTalking();

		game.draw();
		if (talking == false || battling == false)
		{
			game.update();
		}

		game.talk();

		game.printMap();
		game.checkChest();
	}
	game.info();
	system("pause");
	return 0;

}