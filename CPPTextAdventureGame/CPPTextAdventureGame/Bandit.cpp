#include "Bandit.h"

Bandit::Bandit(char typeID)//constructor
{
	spawn(typeID);//type, x and y

	//for battle func
	m_HealthP = new int(25);
	m_StaminaP = new int(7);
	m_LuckP = new int(5);
	m_DmgP = new int(5);

	//for talk func
	m_PersuasionP = new int(2);
	m_IntimidationP = new int(1);
	m_DeceitP = new int(3);
}

void Bandit::update(char map[SIZE][SIZE]) {}//update x y