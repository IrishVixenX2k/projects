#pragma once
#ifndef CHEST_H
#define CHEST_H
#define SIZE 26
//#define FREE 'x'
#include <iostream>
#include "GameObject.h"
#include <ctime>
#include <string>

using namespace std;

class Chest: GameObject {
public:
	//Constructor
	Chest();



	//function declarations
	//void spawn(char typeID = 'C');//sets Chest ID to C
	void Chest::update(char map[SIZE][SIZE]);
	//char getId();//returns pointer to typeID
	//int getX();//returns  pointer to x coord
	//int getY();//returns pointer to y coord

	//destructor
	//~Chest();


//protected variables
protected:
	//int* m_XP;
	//int* m_YP;
	//char* m_TypeIDP;
};

#endif