//
//  CollisionManager.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 28/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#include "CollisionManager.h"
#include "Collision.h"
#include "Player.h"
#include "Enemy.h"
#include "ObstacleObject.h"
#include "BulletHandler.h"
#include "TileLayer.h"

void CollisionManager::checkPlayerEnemyBulletCollision(Player* pPlayer)
{
    SDL_Rect* pRect1 = new SDL_Rect();
    pRect1->x = pPlayer->getPosition().getX();
    pRect1->y = pPlayer->getPosition().getY();
    pRect1->w = pPlayer->getWidth();
    pRect1->h = pPlayer->getHeight();
    
    for(int i = 0; i < TheBulletHandler::Instance()->getEnemyBullets().size(); i++)
    {
        EnemyBullet* pEnemyBullet = TheBulletHandler::Instance()->getEnemyBullets()[i];
        
        SDL_Rect* pRect2 = new SDL_Rect();
        pRect2->x = pEnemyBullet->getPosition().getX();
        pRect2->y = pEnemyBullet->getPosition().getY();
        
        pRect2->w = pEnemyBullet->getWidth();
        pRect2->h = pEnemyBullet->getHeight();
        
        if(RectRect(pRect1, pRect2))
        {
            if(!pPlayer->dying() && !pEnemyBullet->dying())
            {
				//if (pEnemyBullet->type() == "Enemy")
				//{
					pEnemyBullet->collision();
				//}
                pPlayer->collision();
            }
        }
        
        delete pRect2;
    }
    
    delete pRect1;
}

void CollisionManager::checkPlayerGhostBulletCollision(Player* pPlayer)
{
	SDL_Rect* pRect1 = new SDL_Rect();
	pRect1->x = pPlayer->getPosition().getX();
	pRect1->y = pPlayer->getPosition().getY();
	pRect1->w = pPlayer->getWidth();
	pRect1->h = pPlayer->getHeight();

	for (int i = 0; i < TheBulletHandler::Instance()->getGhostBullets().size(); i++)
	{
		GhostBullet* pGhostBullet = TheBulletHandler::Instance()->getGhostBullets()[i];

		SDL_Rect* pRect2 = new SDL_Rect();
		pRect2->x = pGhostBullet->getPosition().getX();
		pRect2->y = pGhostBullet->getPosition().getY();

		pRect2->w = pGhostBullet->getWidth();
		pRect2->h = pGhostBullet->getHeight();

		if (RectRect(pRect1, pRect2))
		{
			if (!pPlayer->dying() && !pGhostBullet->dying())
			{
				pGhostBullet->collision();
				pPlayer->collision();
			}
		}

		delete pRect2;
	}

	delete pRect1;
}

void CollisionManager::checkEnemyPlayerBulletCollision(const std::vector<GameObject *> &objects)
{
    for(int i = 0; i < objects.size(); i++)
    {
        GameObject* pObject = objects[i];
        
        for(int j = 0; j < TheBulletHandler::Instance()->getPlayerBullets().size(); j++)
        {
            if(pObject->type() != std::string("Enemy") || !pObject->updating())
            {
                continue;
            }
            
            SDL_Rect* pRect1 = new SDL_Rect();
            pRect1->x = pObject->getPosition().getX();
            pRect1->y = pObject->getPosition().getY();
            pRect1->w = pObject->getWidth();
            pRect1->h = pObject->getHeight();
            
            PlayerBullet* pPlayerBullet = TheBulletHandler::Instance()->getPlayerBullets()[j];
            
            SDL_Rect* pRect2 = new SDL_Rect();
            pRect2->x = pPlayerBullet->getPosition().getX();
            pRect2->y = pPlayerBullet->getPosition().getY();
            pRect2->w = pPlayerBullet->getWidth();
            pRect2->h = pPlayerBullet->getHeight();
            
            if(RectRect(pRect1, pRect2))
            {
                if(!pObject->dying() && !pPlayerBullet->dying())
                {
                    pPlayerBullet->collision();
                    pObject->collision();
                }
                
            }
            
            delete pRect1;
            delete pRect2;
        }
    }
}

void CollisionManager::checkGhostBulletPlayerBulletCollision(const std::vector<GameObject *> &objects)
{
	for (int i = 0; i < TheBulletHandler::Instance()->getGhostBullets().size(); i++)
	{
		for (int j = 0; j < TheBulletHandler::Instance()->getPlayerBullets().size(); j++)
		{

			GhostBullet* pGhostBullet = TheBulletHandler::Instance()->getGhostBullets()[i];

			SDL_Rect* pRect1 = new SDL_Rect();
			pRect1->x = pGhostBullet->getPosition().getX();
			pRect1->y = pGhostBullet->getPosition().getY();
			pRect1->w = pGhostBullet->getWidth();
			pRect1->h = pGhostBullet->getHeight();

			PlayerBullet* pPlayerBullet = TheBulletHandler::Instance()->getPlayerBullets()[j];

			SDL_Rect* pRect2 = new SDL_Rect();
			pRect2->x = pPlayerBullet->getPosition().getX();
			pRect2->y = pPlayerBullet->getPosition().getY();
			pRect2->w = pPlayerBullet->getWidth();
			pRect2->h = pPlayerBullet->getHeight();

			if (RectRect(pRect1, pRect2))
			{
				if (!pGhostBullet->dying() && !pPlayerBullet->dying())
				{
					pPlayerBullet->collision();
					pGhostBullet->collision();
				}

			}

			delete pRect1;
			delete pRect2;
		}
	}
}



void CollisionManager::checkPlayerEnemyCollision(Player* pPlayer, const std::vector<ShooterObject*> &objects)
{
    SDL_Rect* pRect1 = new SDL_Rect();
    pRect1->x = pPlayer->getPosition().getX();
    pRect1->y = pPlayer->getPosition().getY();
    pRect1->w = pPlayer->getWidth();
    pRect1->h = pPlayer->getHeight();
    
    for(int i = 0; i < objects.size(); i++)
    {
        if(objects[i]->type() != std::string("Enemy") || !objects[i]->updating())
        {
            continue;
        }
        
        SDL_Rect* pRect2 = new SDL_Rect();
        pRect2->x = objects[i]->getPosition().getX();
        pRect2->y = objects[i]->getPosition().getY();
        pRect2->w = objects[i]->getWidth();
        pRect2->h = objects[i]->getHeight();
        
        if(RectRect(pRect1, pRect2))
        {
            if(!objects[i]->dead() && !objects[i]->dying())
            {
                pPlayer->collision();
            }
        }
        
        delete pRect2;
    }
    
    delete pRect1;
}

void CollisionManager::checkPlayerObstacleCollision(Player* pPlayer, const std::vector<ObstacleObject*> &obstacles)
{
	SDL_Rect* pRect1 = new SDL_Rect();
	pRect1->x = pPlayer->getPosition().getX();
	pRect1->y = pPlayer->getPosition().getY();
	pRect1->w = pPlayer->getWidth();
	pRect1->h = pPlayer->getHeight();

	for (int i = 0; i < obstacles.size(); i++)
	{
		if (obstacles[i]->type() != std::string("Obstacle") || !obstacles[i]->updating())
		{
			continue;
		}

		SDL_Rect* pRect2 = new SDL_Rect();
		pRect2->x = obstacles[i]->getPosition().getX();
		pRect2->y = obstacles[i]->getPosition().getY();
		pRect2->w = obstacles[i]->getWidth();
		pRect2->h = obstacles[i]->getHeight();

		if (RectRect(pRect1, pRect2))
		{
			if (!obstacles[i]->dead() && !obstacles[i]->dying())
			{
				pPlayer->collision();
				obstacles[i]->collision();

			}
		}

		delete pRect2;
	}

	delete pRect1;
}

void CollisionManager::checkPlayerTileCollision(Player* pPlayer, const std::vector<TileLayer*>& collisionLayers)
{
    for(std::vector<TileLayer*>::const_iterator it = collisionLayers.begin(); it != collisionLayers.end(); ++it)
    {
        TileLayer* pTileLayer = (*it);
        std::vector<std::vector<int>> tiles = pTileLayer->getTileIDs();
        
        Vector2D layerPos = pTileLayer->getPosition();
        
        int x, y, tileColumn, tileRow, tileid = 0;
        
        x = layerPos.getX() / pTileLayer->getTileSize();
        y = layerPos.getY() / pTileLayer->getTileSize();
        
        if(pPlayer->getVelocity().getX() >= 0 || pPlayer->getVelocity().getY() >= 0)
        {
            tileColumn = ((pPlayer->getPosition().getX() + pPlayer->getWidth()) / pTileLayer->getTileSize());
            tileRow = ((pPlayer->getPosition().getY() + pPlayer->getHeight()) / pTileLayer->getTileSize());
            tileid = tiles[tileRow + y][tileColumn + x];
        }
        else if(pPlayer->getVelocity().getX() < 0 || pPlayer->getVelocity().getY() < 0)
        {
            tileColumn = pPlayer->getPosition().getX() / pTileLayer->getTileSize();
            tileRow = pPlayer->getPosition().getY() / pTileLayer->getTileSize();
            tileid = tiles[tileRow + y][tileColumn + x];
        }
        
        if(tileid != 0)
        {
            pPlayer->collision();
        }
    }
}