#include "Game.h"

//for dialogue between player and enemies
void Game::talk()
{
	//cycles through vector of objects
	for (iterEnemies = enemies.begin(); iterEnemies != enemies.end(); iterEnemies++)
	{
		//if player x and y matches enemy object x and y
		if (player != NULL && (*iterEnemies)->getId() != 'P' && (*iterEnemies)->getId() != FREE && (*iterEnemies)->getX() == player->getX() && (*iterEnemies)->getY() == player->getY())
		{
			if (night == true)
			{
				battle();
			}
			talking = true;
			cout << "--------------------------------------------------------------------\n";
			
			//checks object ID and sets name for battle for each object
			if ((*iterEnemies)->getId() == 'H')
			{
				enemyType = "Harpy";
				harpyTalk();//call harpy dialogue
			}
			else if ((*iterEnemies)->getId() == 'G')
			{
				enemyType = "Goblin";
				goblinTalk();//call goblin dialogue
			}
			else
			{
				enemyType = "Bandit";
				banditTalk();//call bandit dialogue
			}
			talking = false;//to ensure bool is set back to false before ending func
		}//end if same position
	}//end for cycle through vector
}

//challed if player is on enemy position
//will start a battle between player and enemy
//ends the game if all enemies or the player has been destroyed
void Game::battle()
{
	//int deadCount = 0;
	int playerDead = 0;
	playerBattleHealth = playerHealth;

		if (player != NULL && (*iterEnemies)->getId() != 'P' && (*iterEnemies)->getId() != FREE && (*iterEnemies)->getX() == player->getX() && (*iterEnemies)->getY() == player->getY())
		{
			cout << "--------------------------------------------------------------------\n";
			cout << "Battle Has Begun!\n";
			battling = true;

			//get stats to be used in battle
			playerDmg = player->getDmg();
			enemyDmg = (*iterEnemies)->getDmg();
			enemyHealth = (*iterEnemies)->getHealth();

			//set name of enemy for cout statements
			if ((*iterEnemies)->getId() == 'H')
			{
				enemyType = "Harpy";
			}
			else if ((*iterEnemies)->getId() == 'G')
			{
				enemyType = "Goblin";
			}
			else
			{
				enemyType = "Bandit";
			}

			//while player is battling
			while (battling == true) {
				cout << "Your health = " << playerBattleHealth << endl;
				cout << enemyType << "'s health = " << enemyHealth << endl;

				cout << "--------------------------------------------------------------------\n";
				cout << enemyType << " poises to attack\n";
				cout << "You read your weapon and 1) Absorb damage 2) Try to dodge attack\n";
				cout << "If dodge attempt fails, attack may deal double damage.\n";
				cout << "Your choice: ";
				int choice;
				cin >> choice;
				cout << "--------------------------------------------------------------------\n";

				//decides player reaction to enemy attack
				if (choice == 1)
				{
					enemyAttack();
				}
				else if (choice == 2)
				{
					playerDodge();
				}
				else
				{
					//loops if wrong input
					while (choice != 1 || choice != 2)
					{
						cout << "Invalid choice, 1) Absorb damage 2) Try to dodge attack\n";
						cout << "Choice: ";
						cin >> choice;
						cout << "--------------------------------------------------------------------\n";
					}//end while for loop input
				}//end while for battle = true

				cout << "Your health = " << playerBattleHealth << endl;
				cout << enemyType << "'s health = " << enemyHealth << endl;
				cout << "--------------------------------------------------------------------\n";
				cout << "You raise your weapon and 1) Standard attack 2) Special attack (luck)\n";
				cout << "Special attack has chance of dealing double damage but may deal no damage\n";
				cout << "Choice: ";
				cin >> choice;
				cout << "--------------------------------------------------------------------\n";

				//choose player attack
				if (choice == 1)
				{
					playerAttack();//calls standard attack
				}
				else if (choice == 2)
				{
					playerSpecialAttack();//calls special attack
				}
				else
				{
					//loop while wrong input
					while (choice != 1 || choice != 2)
					{
						cout << "Invalid choice, 1) Standard attack 2) Special attack (luck)\n";
						cout << "Choice: ";
						cin >> choice;
					}
				}

				//checks if enemy is dead and adds to counter
				if (enemyHealth <= 0)
				{
					cout << "The " << enemyType << " has died.\n";
					deadCount++;
					battling = false;
					(*iterEnemies)->~Enemy();//call destructor
				}

				//checks if player is dead and ends game
				if (playerBattleHealth <= 0) {
					cout << "You Have Died. Game Over!\n";
					player->~Enemy();
					info();
					system("pause");
					system("exit");
				}
			}//end while for is battling
		}//end if


	//}//end for loop that cycles through vector of objects

	if (deadCount == 5) {
		cout << "All Enemies dead, You Win!\n";
		info();
		system("pause");
		system("exit");

	}

}

//draws the objects to the map
void Game::draw()
{
	for (iterEnemies = enemies.begin(); iterEnemies != enemies.end(); iterEnemies++)
	{
		(*iterEnemies)->draw();
	}

}

//displays the information about the player and enemy
void Game::info()
{
	cout << "Player Skill Levels\n"
		<< "Health: "<< playerHealth << endl
		<< "Stamina: " << playerStamina << endl
		<< "Luck: " << playerLuck << endl
		<< "Persuasion: " << playerPersuasion << endl
		<< "Intimidation: " << playerIntimidation << endl
		<< "Deceit: " << playerDeceit << endl;

}

//initialises the player, enemies and chests and places them in a vector
void Game::init()
{
	Enemy *playerP = new Player('P');
	Enemy *goblinP = new Goblin('G');
	Enemy *harpyP = new Harpy('H');
	Enemy *harpy2P = new Harpy('H');
	Enemy *banditP = new Bandit('B');
	Enemy *bandit2P = new Bandit('B');


	GameObject *chestP1 = new GameObject('C');
	GameObject *chestP2 = new GameObject('C');
	GameObject *chestP3 = new GameObject('C');
	GameObject *chestP4 = new GameObject('C');
	GameObject *chestP5 = new GameObject('C');
	GameObject *chestP6 = new GameObject('C');

	enemies.push_back(playerP);
	enemies.push_back(goblinP);
	enemies.push_back(harpyP);
	enemies.push_back(harpy2P);
	enemies.push_back(banditP);
	enemies.push_back(bandit2P);

	chests.push_back(chestP1);
	chests.push_back(chestP2);
	chests.push_back(chestP3);
	chests.push_back(chestP4);
	chests.push_back(chestP5);
	chests.push_back(chestP6);



	for (int j = 0; j < SIZE; j++)
	{
		for (int i = 0; i < SIZE; i++)
		{
			map[i][j] = FREE;
		}
	}

	//places objects on map
	map[playerP->getX()][playerP->getY()] = playerP->getId();
	map[goblinP->getX()][goblinP->getY()] = goblinP->getId();
	map[harpyP->getX()][harpyP->getY()] = harpyP->getId();
	map[harpy2P->getX()][harpy2P->getY()] = harpy2P->getId();
	map[banditP->getX()][banditP->getY()] = banditP->getId();
	map[bandit2P->getX()][bandit2P->getY()] = bandit2P->getId();

	map[chestP1->getX()][chestP1->getY()] = chestP1->getId();
	map[chestP2->getX()][chestP2->getY()] = chestP2->getId();
	map[chestP3->getX()][chestP3->getY()] = chestP3->getId();
	map[chestP4->getX()][chestP4->getY()] = chestP4->getId();
	map[chestP5->getX()][chestP5->getY()] = chestP5->getId();
	map[chestP6->getX()][chestP6->getY()] = chestP6->getId();

	//find player object in vector and set as 'player' for later use
	for (iterEnemies = enemies.begin(); iterEnemies != enemies.end(); iterEnemies++)
	{
		if ((*iterEnemies)->getId() == 'P')
		{
			player = (*iterEnemies);
		}
	}

	printMap();//calls print map func

	//sets player stats for use in battle and dialogue, as well as rewards from chests
	playerHealth = player->getHealth();
	playerStamina = player->getStamina();
	playerLuck = player->getLuck();
	playerPersuasion = player->getPersuasion();
	playerIntimidation = player->getIntimidation();
	playerDeceit = player->getDeceit();

	cout << "--------------------------------------------------------------------\n"
		<< "PLEASE USE FULL SCREEN FOR BEST GAME EXPERIENCE\n"
		<< "--------------------------------------------------------------------\n"
		<< "You awaken to the sound of birds chirping in the pink light of dawn.\n"
		<< "The soft grass brushes against your face as you stand up and take in the\n"
		<< "vast expanse of rocky mountains surrounding you\n"
		<< "P = player, C = chest, B = Bandit, H = Harpy, G = goblin\n";
	deadCount = 0;
	victory = false;
	hour = 0;
	//newHour = 0;
}
//prints the map
void Game::printMap()
{
	for (int j = 0; j < SIZE; j++)
	{
		for (int i = 0; i < SIZE; i++)
		{
			cout << map[i][j] << FREE;
		}
		cout << endl;
	}
}

//updates the map with new position
void Game::update()
{
	//for enemy and player objects
	for (iterEnemies = enemies.begin(); iterEnemies != enemies.end(); iterEnemies++)
	{
		(*iterEnemies)->update(map);
	}

	//for chest objects
	for (iterChest = chests.begin(); iterChest != chests.end(); iterChest++)
	{
		(*iterChest)->update(map);
	}

	hour++;
	if (hour == 12)
	{
		if (night == false)
		{
			night = true;
			cout << "--------------------------------------------------------------------\n"
				<< "The sun sets and night falls. Be wary, foes become hostile at night and will not converse\n"
				<< "--------------------------------------------------------------------\n";
			hour = 0;
		}
		else
		{
			night = false;
			cout << "--------------------------------------------------------------------\n"
				<< "The sun rises. It is now daytime. Foes will not be hostile while it is bright\n"
				<< "--------------------------------------------------------------------\n";
			hour = 0;
		}

	}

	if (deadCount == 5) {
		cout << "All Enemies dead, You Win!\n";
		victory = true;
		return;

	}
}


//bounds checking to stop player moving off map
bool Game::check(int x, int y)
{
	bool validMove = true;
	if (x >= SIZE || x < 0 || y >= SIZE || y < 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

//checks if player is on a chest
void Game::checkChest()
{
	for (iterChest = chests.begin(); iterChest != chests.end(); iterChest++)
	{
		if ((*iterChest)->getId() != FREE && (*iterChest)->getX() == player->getX() && (*iterChest)->getY() == player->getY())
		{
			chestRiddle();//calls func for riddle

				if (correct == true)//if riddle answered correctly
				{
					chestReward();//call func for picking reward
				}//end if correct
		}//end same coord
	}//end for cycle through vector
}//end checkChest

//picks a random riddle for chest
void Game::chestRiddle()
{
	cout << "--------------------------------------------------------------------\n"
		<< "You have discovered a hidden treasure chest!\n";

	srand(static_cast<unsigned int>(time(NULL)));
	int pickRiddle = rand() % 5;
	int choice;

	cout << "--------------------------------------------------------------------\n"
		<< "To unlock the secrets within you must first answer this riddle\n"
		<< "If you answer incorrectly, your life essence (health) will be weakened\n"
		<< "--------------------------------------------------------------------\n";

	while (correct == false || incorrect == false)
	{
		//case switch to decide which riddle
		switch (pickRiddle) {
		case 0://1st riddle
			cout << "I gurgle but never speak, run but never walk, have a bed but never sleep. What am I?\n"
				<< "1)I am a baby, 2)I am an ocean, 3)I am a river, 4)LOL what?\n"
				<< "Choice: ";
			cin >> choice;
			cout << "--------------------------------------------------------------------\n";
			if (choice == 3)//answer correct
			{
				cout << "The chest glows with green light and clicks open\n";
				correct = true;
			}
			else if (choice == 1 || choice == 2 || choice == 4)//answer incorrect
			{
				cout << "The chest glows with a red light and you feel a shot of pain through you chest\n";
				playerHealth -= 10;//take from player health
				incorrect = true;
				(*iterChest)->~GameObject();//destructor
			}
			else
			{
				cout << "Invalid choice, please answer again\n";//remain in while loop
			}
			break;
		case 1://2nd riddle
			cout << "I am not alive but I can grow. I do not have lungs but I need air to survive. What am I?\n"
				<< "1)I am water, 2)I am fire, 3)I am air, 4)I am confused\n"
				<< "Choice: ";
			cin >> choice;
			cout << "--------------------------------------------------------------------\n";
			if (choice == 2)//correct answer
			{
				cout << "The chest glows with green light and clicks open\n";
				correct = true;
			}
			else if (choice == 4)//special answer
			{
				cout << "The chest laughs, 'Good one!'\n";
				cout << "The chest glows with green light and clicks open\n";
				correct = true;
			}
			else if (choice == 1 || choice == 3)//incorrect answers
			{
				cout << "The chest glows with a red light and you feel a shot of pain through you chest\n";
				playerHealth -= 10;//take from player health
				incorrect = true;
				(*iterChest)->~GameObject();//destructor
			}
			else
			{
				cout << "Invalid choice, please answer again\n";//remain in while loop
			}
			break;
		case 2://3rd riddle
			cout << "You bury me when I�m alive and dig me up when I�m dead. What am I?\n"
				<< "1)I am a plant, 2)I am a zombie, 3)I am a murder victim, 4)I hate riddles!\n"
				<< "Choice: ";
			cin >> choice;
			cout << "--------------------------------------------------------------------\n";
			if (choice == 1)//correct answer
			{
				cout << "The chest glows with green light and clicks open\n";
				correct = true;
			}
			else if (choice == 1 || choice == 3 || choice == 4)//incorrect answer
			{
				cout << "The chest glows with a red light and you feel a shot of pain through you chest\n";
				playerHealth -= 10;//take from player health
				incorrect = true;
				(*iterChest)->~GameObject();//destructor
			}
			else
			{
				cout << "Invalid choice, please answer again\n";//remain in while loop
			}
			break;
		case 3://4th riddle
			cout << "I have an eye but cannot see. I am fast but I have no limbs. What am I?\n"
				<< "1)I am a wizard, 2)I am a serpent, 3)I am a hurricane, 4)I am not conforming to your expectations\n"
				<< "Choice: ";
			cin >> choice;
			cout << "--------------------------------------------------------------------\n";
			if (choice == 3)//correct answer
			{
				cout << "The chest glows with green light and clicks open\n";
				correct = true;
			}
			else if (choice == 1 || choice == 2 || choice == 4)//incorrect answers
			{
				cout << "The chest glows with a red light and you feel a shot of pain through you chest\n";
				playerHealth -= 10;//take from player health
				incorrect = true;
				(*iterChest)->~GameObject();//destructor
			}
			else
			{
				cout << "Invalid choice, please answer again\n";//remain in while loop
			}
			break;
		case 4://5th riddle
			cout << "Everyone has me except for a few.\n"
				<< "You may not be able to identify me but I can always identify you. What am I?\n"
				<< "1)I am your fingerprints, 2)I am creepy, 3)I am a ufo, 4)I don't like problem solving\n"
				<< "Choice: ";
			cin >> choice;
			cout << "--------------------------------------------------------------------\n";
			if (choice == 1)//correct answer
			{
				cout << "The chest glows with green light and clicks open\n";
				correct = true;
			}
			else if (choice == 2 || choice == 3 || choice == 4)//incorrect answers
			{
				cout << "The chest glows with a red light and you feel a shot of pain through you chest\n";
				playerHealth -= 10;//take from player health
				incorrect = true;
				(*iterChest)->~GameObject();//destructor
			}
			else
			{
				cout << "Invalid choice, please answer again\n";//remain in while loop
			}
			break;
		default://6th riddle
			cout << "Sometimes I walk in front of you. Sometimes I walk behind you.\n"
				<< "It is only in the dark that I ever leave you. What am I?\n"
				<< "1)I am a confused stalker, 2)I am your shadow, 3)I am your neighbour's dog, 4)I don't understand\n"
				<< "Choice: ";
			cin >> choice;
			cout << "--------------------------------------------------------------------\n";
			if (choice == 2)//correct answer
			{
				cout << "The chest glows with green light and clicks open\n";
				correct = true;
			}
			else if (choice == 1 || choice == 3 || choice == 4)//incorrect answers
			{
				cout << "The chest glows with a red light and you feel a shot of pain through you chest\n";
				playerHealth -= 10;//take from player health
				incorrect = true;
				(*iterChest)->~GameObject();//destructor
			}
			else
			{
				cout << "Invalid choice, please answer again\n";//remain in while loop
			}
			break;
		}//end case switch
		return;//break out of function to avoid endless loop
	}//end while correct
}

//pick a random reward for chest
void Game::chestReward()
{
	srand(static_cast<unsigned int>(time(NULL)));
	int item = rand() % 6;

	cout << "--------------------------------------------------------------------\n";
	switch (item) {
	case 0://health increase
		playerHealth += 10;
		cout << "You found a health potion\n"
			<< "Your health has been increased by 10!\n"
			<< "Your health is now " << playerHealth << endl;
		(*iterChest)->~GameObject();
		break;
	case 1://stamina increase
		playerStamina += 3;
		cout << "You found a stamina potion\n"
			<< "Your Stamina has been increased by 3!\n"
			<< "Your stamina is now " << playerStamina << endl;
		(*iterChest)->~GameObject();
		break;
	case 2://luck increase
		playerLuck += 2;
		cout << "You found a lucky charm\n"
			<< "Your lcuk has been increased by 2!\n"
			<< "Your luck is now " << playerLuck << endl;
		(*iterChest)->~GameObject();
		break;
	case 3://persuasion increase
		playerPersuasion += 1;
		cout << "You found a book\n"
			<< "Your persuasion has increase by 1!\n"
			<< "Your persuasion is now " << playerPersuasion << endl;
		(*iterChest)->~GameObject();
		break;
	case 4://intimidation increase
		playerIntimidation += 1;
		cout << "You found an eyepatch\n"
			<< "Your deceit has intimidation by 1\n!"
			<< "Your intimidation is now " << playerIntimidation << endl;
		(*iterChest)->~GameObject();
		break;
	default://deceit increase
		playerDeceit += 1;
		cout << "You found what appears to be nothing\n"
			<< "Your deceit has increase by 1\n!"
			<< "Your deceit is now " << playerDeceit << endl;
		(*iterChest)->~GameObject();
		break;
	}//end case switch
	cout << "--------------------------------------------------------------------\n";
	return;//break out of function to avoid endless loop
}

 //checks to see if player is talking
bool Game::isTalking()
{
	if (talking == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//checks to see if player is battling
bool Game::isBattling()
{
	if (battling == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//check to see if it is night
bool Game::isNight()
{
	if (night == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Game::getVictory()
{
	if (victory == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//if player chooses to dodge in battle
void Game::playerDodge()
{
	//when enemy attacks player
	//decide player dodge chance if player stamina is higher
	if (playerStamina >= (*iterEnemies)->getStamina()) {
		srand(static_cast<unsigned int>(time(NULL)));
		dmgMultiplier = rand() % 2 + 1;
		switch (dmgMultiplier) {
		case 1://player dodges
			cout << "You dodged the " << enemyType << "'s attack\n";
			break;
		default://player dodge attempt failed
			cout << enemyType << " attack lands dealing " << enemyDmg << "damage\n";
			playerBattleHealth -= enemyDmg;
			break;
		}//end case switch
	}
	else//if player stamina lower
	{
		srand(static_cast<unsigned int>(time(NULL)));
		dmgMultiplier = rand() % 4 + 1;

		switch (dmgMultiplier) {//health increase
		case 1://dodge attempt failed, normal damage
			cout << enemyType << " attack lands dealing " << enemyDmg << "damage\n";
			playerBattleHealth -= enemyDmg;
			break;
		case 2://dodge attempt failed, normal damage
			cout << enemyType << " attack lands dealing " << enemyDmg << "damage\n";
			playerBattleHealth -= enemyDmg;
			break;
		case 3://dodge attempt failed, double damage
			cout << enemyType << " special attack lands dealing " << enemyDmg * 2 << "damage\n";
			playerBattleHealth -= enemyDmg * 2;
			break;
		default://dodge attempt succeeded
			cout << "You dodged the " << enemyType << "'s attack\n";
			break;
		}//end case switch
	}//end else

	 //}
}//end playerDodge

//enemy attack fun for battle
void Game::enemyAttack()
{
	//decide enemy damage using luck
	//if enemy luck is higher than players
	if ((*iterEnemies)->getLuck() > playerLuck) {
		srand(static_cast<unsigned int>(time(NULL)));
		int dmgMultiplier = rand() % 4 + 1;

		switch (dmgMultiplier) {
		case 1://enemy deals normal damage
			cout << enemyType << "'s attack lands dealing " << enemyDmg << "damage\n";
			playerBattleHealth -= enemyDmg;
			break;
		case 2://enemy deals normal damage
			cout << enemyType << "'s attack lands dealing " << enemyDmg << "damage\n";
			playerBattleHealth -= enemyDmg;
			break;
		case 3://enemy deals normal damage
			cout << enemyType << "'s attack lands dealing " << enemyDmg << "damage\n";
			playerBattleHealth -= enemyDmg;
			break;
		default://enemy deals double damage
			cout << enemyType << " gets lucky and lands an attack dealing " << enemyDmg * 2 << "damage\n";
			playerBattleHealth -= enemyDmg * 2;
			break;
		}//end case switch
	}//end if
	else
	{
		//if player luck higher, enemy just deals normal damage
		cout << enemyType << "'s attack lands dealing " << enemyDmg << "damage\n";
		playerBattleHealth -= enemyDmg;
	}//end if else
}//end enemyAttack

//player standard attack for battle
void Game::playerAttack()
{
	//decide enemy dodge chance
	//if enemy stamina higher
	if ((*iterEnemies)->getStamina() > playerStamina) {
		srand(static_cast<unsigned int>(time(NULL)));
		//int newEnemyDmg;
		dmgMultiplier = rand() % 4 + 1;
		switch (dmgMultiplier) {//deal standard damage
		case 1:
			cout << "You swing your sword hitting the " << enemyType << " and dealing " << playerDmg << "damage\n";
			enemyHealth -= playerDmg;
			break;
		case 2://standard damage
			cout << "You swing your sword hitting the " << enemyType << " and dealing " << playerDmg << "damage\n";
			enemyHealth -= playerDmg;
			break;
		case 3://standard damage
			cout << "You swing your sword hitting the " << enemyType << " and dealing " << playerDmg << "damage\n";
			enemyHealth -= playerDmg;
			break;
		default://enemy dodged
			cout << "The enemy dodged your attack\n";
			break;
		}//end case switch

	}
	else
	{
		//if player stamina higher, do standard damage
		cout << "You swing your sword hitting the " << enemyType << " and dealing " << playerDmg << "damage\n";
		enemyHealth -= playerDmg;
	}//end else
}

//player special attack for battle
void Game::playerSpecialAttack()
{
	//decide player damage using luck
	//if player luck higher
	if (playerLuck >= (*iterEnemies)->getLuck()) {
		srand(static_cast<unsigned int>(time(NULL)));
		int dmgMultiplier = rand() % 4 + 1;

		switch (dmgMultiplier) {//standard damage
		case 1:
			cout << "You swing your sword hitting the " << enemyType << " and dealing " << playerDmg << "damage\n";
			enemyHealth -= playerDmg;
			break;
		case 2://standard damage
			cout << "You swing your sword hitting the " << enemyType << " and dealing " << playerDmg << "damage\n";
			enemyHealth -= playerDmg;
			break;
		case 3://double damage
			cout << "You get lucky and land an attack dealing " << playerDmg * 2 << "damage\n";
			enemyHealth -= playerDmg * 2;
			break;
		default://enemy dodges
			cout << enemyType << " dodges your attack\n";
			break;
		}//end case switch
	}//end if
	else
	{
		//enemy luck higher
		srand(static_cast<unsigned int>(time(NULL)));
		int dmgMultiplier = rand() % 5 + 1;

		switch (dmgMultiplier) {
		case 1://standard damage
			cout << "You swing your sword hitting the " << enemyType << " and dealing " << playerDmg << "damage\n";
			enemyHealth -= playerDmg;
			break;
		case 2://standard damage
			cout << "You swing your sword hitting the " << enemyType << " and dealing " << playerDmg << "damage\n";
			enemyHealth -= playerDmg;
			break;
		case 3://double damage
			cout << "You get lucky and land an attack dealing " << playerDmg * 2 << "damage\n";
			enemyHealth -= playerDmg * 2;
			break;
		case 4://enemy dodges
			cout << enemyType << " dodges your attack\n";
			break;
		default://enemy doges
			cout << enemyType << " dodges your attack\n";
			break;
		}//end case switch
	}//end if else
}

//harpy dialogue for encounter
void Game::harpyTalk()
{
	int choice;

	cout << "You step inside the cave and hear a croak from above you\n"
		<< "You look up to find a harpy peering quizically back\n"
		<< "Harpy: Who dares enter my lair? Speak human!\n";
	cout << "--------------------------------------------------------------------\n";

	while (talking == true)//runs if player talking
	{
		cout << "1)Persuade, 2)Intimidate, 3)Deceit\n" << "Your choice: ";
		cin >> choice;
		cout << "--------------------------------------------------------------------\n";

		switch (choice) {
		case 1://persuade, harpy will be persuaded
			cout << "You: I mean you no harm. I am looking for a demon in this area, it would be wise for you to leave for your own safety\n"
				<< "You: You may return once the demon has been dealt with. You have my word no harm will come to this lair\n";
			cout << "--------------------------------------------------------------------\n";
			cout << "Harpy : I wish only to live in peace, I will leave until the demon has been dealt with\n"
				<< "You successfuly persuade the harpy to leave the area without conflict\n"
				<< "Your persuasion has increased by 1!\n";
			playerPersuasion += 1;//increase player persuasion
			(*iterEnemies)->~Enemy();//call destructor
			talking = false;
			deadCount++;
			break;
		case 2://intimidate
			cout << "You: I have no time for your petty concerns. There is a great evil in this land that must be purged\n"
				<< "You: Step out of my way or I will put you down, scum!\n";
			cout << "--------------------------------------------------------------------\n";
			
			//if player intimidation higher, player will successfully intimidate
			if (playerIntimidation >= (*iterEnemies)->getIntimidation())
			{
				cout << "Harpy: I am no fool, I do not wish to test your might in battle. I will leave for now\n"
					<< "You successfully initimidate the harpy and she leaves the area without conflict\n"
					<< "Your intimidation has increased by 1!\n";
				playerIntimidation += 1;//increase intimidation
				(*iterEnemies)->~Enemy();//call destructor
				talking = false;
				deadCount++;
			}
			else
			{
				//if harpy intimidation higher
				cout << "Harpy: Foolish human. Your threats hold no substance in my land\n"
					<< "Harpy: You may not have wings, but I will make your spirit fly!\n";
				talking = false;
				battle();//call battle
			}
			break;
		case 3://lie to harpy
			cout << "You: There is a demon headed straight for you, run while you have time!\n";
			cout << "--------------------------------------------------------------------\n";
			
			//if player deceit higher
			if (playerDeceit >= (*iterEnemies)->getDeceit())
			{
				cout << "Harpy: I want no part in this, demons are foul creatures!"
					<< "Harpy: If you have any smarts in that human head of yours, you will run too!\n"
					<< "You successfully deceive the harpy and she leaves the area without coflict\n"
					<< "Your deceit has increased by 1!\n";
				playerDeceit += 1;//increase player deceit
				(*iterEnemies)->~Enemy();//call destructor
				talking = false;
				deadCount++;
			}
			else
			{
				//if harpy deceit higher
				cout << "Harpy: You think you can lie to a harpy? I will tear your lying tongue out\n";
				talking = false;
				battle();//call battle
			}
			break;
		//removed due to enemy icon disappearing
		/*case 4:
			cout << "--------------------------------------------------------------------\n";
			cout << "You slowly back away from the harpy's lair\n";
			break;*/
		default://loop while wrong input
			cout << "--------------------------------------------------------------------\n";
			cout << "Invalid choice\n";
			break;
		}
		return;
	}//end while
}//end harpyTalk func

//dialogue for bandit encounter
void Game::banditTalk()
{
	int choice;
	cout << "You step on to the road and hear a twig snap behind you\n"
		<< "A shrouded figure charges you, knocking you to the ground and revealing what appears to be a dagger hidden beneath his cloak\n"
		<< "Bandit: That's some shiny armour you've got there! I bet that would fetch me a pretty penny\n";

	while (talking = true)
	{
		cout << "1)Persuade, 2)Intimidate, 3)Deceit\n" << "Your choice: ";
		cin >> choice;
		cout << "--------------------------------------------------------------------\n";
		switch (choice) {
		case 1://persuade
			//player persuasion higher
			if (playerPersuasion >= (*iterEnemies)->getPersuasion())
			{
				cout << "You: I mean you no harm. I am looking for a demon in this area, it would be wise for you to leave for your own safety\n"
					<< "You: You may return once the demon has been dealt with. You have my word no harm will come to this lair\n";

				cout << "--------------------------------------------------------------------\n";
				cout << "Bandit : Demon? Can't do much with a demon knocking about. I like me stealing but even I have standards\n"
					<< "You successfuly persuade the bandit to leave the area without conflict\n"
					<< "Your persuasion has increased by 1!\n";
				playerPersuasion += 1;//increase persuasion
				(*iterEnemies)->~Enemy();//call destructor
				talking = false;
				deadCount++;
			}
			else
			{
				//bandit persuasion higher
				cout << "Bandit: I might not know how to read any of them fancy books but I know a story when I hears one\n"
					<< "Bandit: How abouts I put a hole between that pretty helmet and that chest plate\n";
				talking = false;
				battle();
			}
			break;
		case 2://intimidate, bandit will always be intimidated
			cout << "You: I have no time for your petty concerns. There is a great evil in this land that must be purged\n"
				<< "You: Step out of my way or I will put you down, scum! Then I'll be wearing that pretty cloak of your's as a scarf!\n";
			cout << "--------------------------------------------------------------------\n";
			cout << "Bandit: My grams gave me this cloak so she did! Nobody insults my poor old grams\n"
				<< "<Bandit: Lucky you there's a demon on the proul or I'd wipes that smug off your face\n"
				<< "Bandit: I'll be back for my shiny armour once that demon is through with you\n"
				<< "You successfully initimidate the bandit and he leaves the area without conflict\n"
				<< "Your intimidation has increased by 1!\n";
			playerIntimidation += 1;//increase intimidation
			(*iterEnemies)->~Enemy();//call destructor
			talking = false;
			deadCount++;
			break;
		case 3://lie to bandit
			cout << "You: There is a demon headed straight for you, run while you have time!\n";
			cout << "--------------------------------------------------------------------\n";
			
			//if player deceit higher
			if (playerDeceit >= (*iterEnemies)->getDeceit())
			{
				cout << "Bandit: Demon! Riches a plenty where demons sleep but this man is no warrior!"
					<< "Bandit: I'll be back to pillage your corpse when this is over\n"
					<< "You successfully deceive the bandit and he leaves the area without coflict\n"
					<< "Your deceit has increased by 1!\n";
				playerDeceit += 1;//increase deceit
				(*iterEnemies)->~Enemy();//call destructor
				talking = false;
				deadCount++;
			}
			else
			{
				//if bandit deceit lower
				cout << "Bandit: Nice try princess, but I was born lying\n"
					<< "Bandit: Nothing like stealing from the corpse of an honest man!\n";
				talking = false;
				battle();//call battle
			}
			break;
		//removed due to bandit icon disappearing from map
		/*case 4:
			cout << "You knee the bandit between the legs while he's admiring your helmet and run\n";
			break;*/
		default:
			cout << "--------------------------------------------------------------------\n";
			cout << "Invalid choice\n";
			break;
		}
		return;//prevent endless loop
	}
}//end banditTalk


//dialogue for goblin
void Game::goblinTalk()
{
	int choice;
	cout << "You step in to a foul smelling cave and hear a garggle from deeper within\n"
		<< "A large, ugly goblin with a burnt face appears from the darkness and scuttles closer\n"
		<< "Goblin: Why is stringy pretty goblin in Merlin's below-low house?\n";

	while (talking == true)
	{
		cout << "1)Persuade, 2)Intimidate, 3)Deceit\n" << "Your choice: ";
		cin >> choice;
		cout << "--------------------------------------------------------------------\n";
		switch (choice) {
		case 1://persuade

			//player persuasion higher
			if (playerPersuasion >= (*iterEnemies)->getPersuasion())
			{
				cout << "You: I mean you no harm. I am looking for a demon in this area, it would be wise for you to leave for your own safety\n"
					<< "You: You may return once the demon has been dealt with. You have my word no harm will come to your... below-low house\n";

				cout << "--------------------------------------------------------------------\n";
				cout << "Goblin: Merlin no like demons, demons are mean and stinky\n"
					<< "Merlin go stay with aunt bessy in the up up high rocks\n"
					<< "You successfuly persuade the goblin to leave the area without conflict\n"
					<< "Your persuasion has increased by 1!\n";
				playerPersuasion += 1;//increase persuasion
				(*iterEnemies)->~Enemy();//call destructor
				talking = false;
				deadCount++;
			}
			else
			{
				cout << "Goblin: Merlin no like your words! Don't say words to Merlin\n"
					<< "Goblin: Merlin show you his shiny stabby stick\n";
				talking = false;
				battle();//call battle
			}
			break;
		case 2://intimidate
			cout << "You: I have no time for your petty concerns. There is a great evil in this land that must be purged\n"
				<< "You: Step out of my way or I will put you down, scum! I'll be using your head for my next puppet show!\n"
				<< "You: Then maybe you'll finally learn to speak properly\n";

			cout << "--------------------------------------------------------------------\n";
			
			//player intimidation higher
			if (playerIntimidation >= (*iterEnemies)->getIntimidation())
			{
				cout << "Goblin: Merlin know when to use words and not stabby sticks\n"
					<< "<Goblin: Merlin will show restraint this time, aunt bessy tell Merlin know when to show restraint\n"
					<< "You successfully initimidate the goblin and he leaves the area without conflict\n"
					<< "Your intimidation has increased by 1!\n";
				playerIntimidation += 1;//increase intimidation
				(*iterEnemies)->~Enemy();//call destructor
				talking = false;
				deadCount++;
			}
			else
			{
				//goblin intimidation higher
				cout << "Goblin: Merlin use your head as second head, stupid!\n";
				talking = false;
				battle();//call battle
			}
			break;
		case 3://lie to goblin
			cout << "You: There is a demon headed straight for you, run while you have time!\n";
			cout << "--------------------------------------------------------------------\n";
			cout << "Goblin: Out of Merlin's way! Merlin no time for stinky demon bullies\n"
				<< "Merlin runs straight at you, knocking you to the ground and disappears through the mouth of the cave\n"
				<< "You successfully deceive the goblin and he leaves the area without coflict\n"
				<< "Your deceit has increased by 1!\n";
			playerDeceit += 1;//increase deceit
			(*iterEnemies)->~Enemy();//call destructor
			talking = false;
			deadCount++;
			break;
		//removed due to goblin icon disappearing from map
		/*case 4:
			cout << "You throw a rock at Merlin, hitting him in the forehead and run away\n";
			break;*/
		default://loop while wrong input
			cout << "--------------------------------------------------------------------\n";
			cout << "Invalid choice\n";
			break;
		}
		return;//prevent endless loop
	}
}//end goblinTalk()
