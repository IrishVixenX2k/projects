<?xml version="1.0" encoding="UTF-8"?>
<tileset name="floorTiles" tilewidth="614" tileheight="376" tilecount="1" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="614" height="376" source="blocks4.png"/>
  <objectgroup draworder="index">
   <object id="2" x="2.5" y="1" width="31.5" height="33"/>
   <object id="3" x="36" y="2" width="31.5" height="32"/>
   <object id="4" x="70" y="3" width="31.5" height="30.5"/>
   <object id="5" x="103.5" y="2" width="33" height="32"/>
  </objectgroup>
 </tile>
</tileset>
