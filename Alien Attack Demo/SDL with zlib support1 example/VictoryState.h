//
//  GameOverState.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 17/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef VICTORYSTATE_H
#define VICTORYSTATE_H

#include <iostream>
#include <vector>
#include "MenuState.h"

class GameObject;

class VictoryState : public MenuState
{
public:

	virtual ~VictoryState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_gameOverID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_gameOverToMain();
	static void s_exitFromVictory();

	static const std::string s_gameOverID;

	std::vector<GameObject*> m_gameObjects;
};

#endif /* defined(__SDL_Game_Programming_Book__GameOverState__) */
