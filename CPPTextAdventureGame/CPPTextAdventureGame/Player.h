
#ifndef PLAYER_H
#define PLAYER_H
#include "Enemy.h"
#include "Game.h"

class Player : public Enemy
{
public:
	//constructor
	Player(char typeID = 'P');
	//updates the players position on the map
	void virtual update(char[SIZE][SIZE]);

};
#endif
