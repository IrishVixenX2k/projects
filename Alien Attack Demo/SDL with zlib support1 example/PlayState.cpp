//
//  PlayState.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 09/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#include <iostream>
#include "PlayState.h"
#include "GameOverState.h"
#include "SoundManager.h"
#include "PauseState.h"
#include "Game.h"
#include "InputHandler.h"
#include "LevelParser.h"
#include "Level.h"
#include "BulletHandler.h"

const std::string PlayState::s_playID = "PLAY";

void PlayState::update()
{
    if(m_loadingComplete && !m_exiting)
    {
        if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE))
        {
            TheGame::Instance()->getStateMachine()->pushState(new PauseState());
        }
        
//        if(TheInputHandler::Instance()->getButtonState(0, 8))
//        {
//            TheGame::Instance()->getStateMachine()->pushState(new PauseState());
//        }
        
        TheBulletHandler::Instance()->updateBullets();
        
        if(TheGame::Instance()->getPlayerLives() == 0)
        {
            TheGame::Instance()->getStateMachine()->changeState(new GameOverState());
        }
        
        if(pLevel != 0)
        {
            pLevel->update();
        }
    }
}

void PlayState::render()
{
    if(m_loadingComplete)
    {
        if(pLevel != 0)
        {
            pLevel->render();
        }

		TheTextureManager::Instance()->drawFrame("lives3", 5, 5, 36, 32, 0, 0, TheGame::Instance()->getRenderer(), 0.0, 255);
		TheTextureManager::Instance()->drawFrame("lives3", 50, 5, 36, 32, 0, 0, TheGame::Instance()->getRenderer(), 0.0, 255);
		TheTextureManager::Instance()->drawFrame("lives3", 95, 5, 36, 32, 0, 0, TheGame::Instance()->getRenderer(), 0.0, 255);
        
        for(int i = 0; i < TheGame::Instance()->getPlayerLives(); i++)
        {
            TheTextureManager::Instance()->drawFrame("lives", i * 45 + 5, 5, 36, 32, 0, 0, TheGame::Instance()->getRenderer(), 0.0, 255);
        }
        
        TheBulletHandler::Instance()->drawBullets();
    }
}

bool PlayState::onEnter()
{
    TheGame::Instance()->setPlayerLives(3);
    
    LevelParser levelParser;
    pLevel = levelParser.parseLevel(TheGame::Instance()->getLevelFiles()[TheGame::Instance()->getCurrentLevel() - 1].c_str());
    
    TheTextureManager::Instance()->load("assets/bullet1.png", "bullet1", TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->load("assets/bullet2.png", "bullet2", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/fireball.png", "fireball", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/bullet3.png", "bullet3", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/tinyGhost.png", "tinyGhost", TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->load("assets/lifeFull.png", "lives", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/lifeHalf.png", "lives2", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/lifeEmpty.png", "lives3", TheGame::Instance()->getRenderer());

	TheSoundManager::Instance()->load("assets/bgmusic1.ogg", "music1", SOUND_MUSIC);

	TheSoundManager::Instance()->playMusic("music1", -1);
    
    if(pLevel != 0)
    {
        m_loadingComplete = true;
    }
    
    std::cout << "entering PlayState\n";
    return true;
}

bool PlayState::onExit()
{
    m_exiting = true;
    
    TheInputHandler::Instance()->reset();
    TheBulletHandler::Instance()->clearBullets();
    
    std::cout << "exiting PlayState\n";
    return true;
}
