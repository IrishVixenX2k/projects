//
//  Eskeletor.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 30/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef GHOST_H
#define GHOST_H

#include "Enemy.h"

class Ghost : public Enemy
{
public:

	virtual ~Ghost() {}

	Ghost() : Enemy()
	{
		m_dyingTime = 50;
		m_health = 1;
		m_moveSpeed = 2;
		m_bulletFiringSpeed = 100;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "ghostexplosion";
				m_currentFrame = 0;
				m_numFrames = 10;
				m_width = 50;
				m_height = 56;
				m_bDying = true;
			}

		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());
			m_velocity.setY(m_moveSpeed);

			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				/*TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(-3, 0));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(3, 0));*/

				TheBulletHandler::Instance()->addGhostBullet(m_position.getX() + 10, m_position.getY() + 30, 30, 23, "tinyGhost", 1, Vector2D(-3, 0));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 10, m_position.getY() + 30, 30, 23, "tinyGhost", 1, Vector2D(3, 0));
				m_bulletCounter = 0;
			}
			m_bulletCounter++;

		}
		else
		{
			m_velocity.setY(0);
			doDyingAnimation();
		}

		ShooterObject::update();
	}
};

class GhostCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Ghost();
	}
};


#endif
