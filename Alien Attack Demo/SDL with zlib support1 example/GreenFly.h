//
//  ShotGlider.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 30/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef GREENFLY_H
#define GREENFLY_H

#include "Enemy.h"

class GreenFly : public Enemy
{
public:

	virtual ~GreenFly() {}

	GreenFly() : Enemy()
	{
		m_bulletFiringSpeed = 100;
		m_moveSpeed = 3;
		m_gap = 250;
	}

	virtual void load(std::unique_ptr<LoaderParams> const &pParams)
	{
		ShooterObject::load(std::move(pParams));

		m_velocity.setX(-m_moveSpeed);
		m_velocity.setY(m_moveSpeed / 2);

		m_maxHeight = m_position.getY() + m_gap;
		//m_minHeight = m_position.getY() - m_gap;
	}

	virtual void update()
	{
		/*if (!m_bDying)
		{
			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(-10, 0));
				m_bulletCounter = 0;
			}

			m_bulletCounter++;
		}
		else
		{
			m_velocity.setX(0);
			doDyingAnimation();
		}*/

		if (!m_bDying)
		{
			if (m_position.getY() >= m_maxHeight)
			{
				m_velocity.setY(-m_moveSpeed);
			}
			/*else if (m_position.getY() <= m_minHeight)
			{
				m_velocity.setY(m_moveSpeed);
			}*/

			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(-10, 0));
				m_bulletCounter = 0;
			}

			m_bulletCounter++;
		}
		else
		{
			m_velocity.setX(0);
			m_velocity.setY(0);
			doDyingAnimation();
		}

		ShooterObject::update();
	}

private:

	int m_maxHeight;
	int m_minHeight;
	int m_gap;
};

class GreenFlyCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new GreenFly();
	}
};


#endif
