//
//  SDLGameObject.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 19/01/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef OBSTACLEOBJECT_H
#define OBSTACLEOBJECT_H

#include <SDL.h>
#include "GameObject.h"

class ObstacleObject : public GameObject
{
public:

	virtual ~ObstacleObject() {}

	virtual void load(std::unique_ptr<LoaderParams> const &pParams);

	virtual void draw();
	virtual void update();

	virtual void clean() {}
	virtual void collision() {}

	virtual std::string type() { return "Obstacle"; }

protected:

	ObstacleObject();

	void doDyingAnimation();

	//int m_bulletFiringSpeed;
	//int m_bulletCounter;
	int m_moveSpeed;

	// how long the death animation takes, along with a counter
	int m_dyingTime;
	int m_dyingCounter;

	// has the explosion sound played?
	bool m_bPlayedDeathSound;
};

#endif /* defined(__SDL_Game_Programming_Book__SDLGameObject__) */
