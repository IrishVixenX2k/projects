#pragma once
#ifndef BANDIT_H
#define BANDIT_H
//#define SIZE 30
#include "Enemy.h"
#include "Game.h"

class Bandit : public Enemy
{
public:
	//sets bandit ID to B
	Bandit(char typeID = 'B');//constructor
	virtual void update(char[SIZE][SIZE]);
};

#endif




