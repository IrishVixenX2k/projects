#include "Goblin.h"

Goblin::Goblin(char typeID)//Goblin constructor
{
	spawn(typeID);
	m_HealthP = new int(50);
	m_StaminaP = new int(3);
	m_LuckP = new int(2);
	m_DmgP = new int(15);

	//for talk func
	m_PersuasionP = new int(2);
	m_IntimidationP = new int(3);
	m_DeceitP = new int(1);
}

//checks if goblin dead
void Goblin::update(char map[SIZE][SIZE]){}